package jsonreader

import (
	"fmt"
	"io/ioutil"
	"os"
)

type JsonFileReader string

func main() {
	fmt.Println(JsonFileReader.CheckFileExist("./config/Settings.json"))
	// if !checkFileExist("Settings.json") {
	// }

}
func (filename JsonFileReader) CheckFileExist() bool {
	fileexist := true
	if _, err := os.Stat(string(filename)); os.IsNotExist(err) {
		fileexist = false
	}
	return fileexist
}

// func createFile (filename string)
// func checkFileExist(filename string) bool {

// }

func ReadJsonFile(jsonfilename string) string {
	filetext, err := ioutil.ReadFile(jsonfilename)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(filetext))
	}

	return "done"
}
